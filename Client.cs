﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7
{
    internal class Client
    {
        public string Name;
        public string Surname;
        public int Age;
        public double Account_balance;

        public Client(string name, string surname, int age, double account_balance)
        {
            Name = name;
            Surname = surname;
            Age = age;
            Account_balance = account_balance;
        }

        public void ReName(string name)
        {
            Name = name;
        }

        public void ReSurname(string surname)
        {
            Surname = surname;
        }
        public void ReAge(int age)
        {
            Age = age;
        }

        public void ReBalance(double account_balance)
        {
            Account_balance = account_balance;
        }
    }
}

