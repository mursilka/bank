﻿using System;
using System.Collections.Generic;

namespace Lesson7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Bank bank = new Bank();
            Console.WriteLine($"Банк {bank.Name_Bank}");
            Console.WriteLine($"Адрес банка {bank.Address}");

            while (true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("Выберите операцию банка:\n" + "1.Добавить клиента.\n" + "2.Удалить клиента.\n" + "3.Изменение информации клиента.\n" + "4.Информация о клиенте.\n" + "5.Информация обо всех клиентах банка.\n");
                int Choice = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("\n");

                if (Choice == 1)
                {
                    Console.WriteLine("Введите имя клиента:");
                    string _name = Console.ReadLine();
                    Console.WriteLine("Введите фамилию клиента:");
                    string _surname = Console.ReadLine();
                    Console.WriteLine("Введите возраст клиента:");
                    int _age = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Введите баланс клиента:");
                    double _balance = Convert.ToDouble(Console.ReadLine());
                    bank.Add(_name, _surname, _age, _balance);

                    Console.WriteLine("Клиенты успешно создан, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }


                if (Choice == 2)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.Remove();

                    Console.WriteLine("Клиент успешно удалён, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 3)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.ReClient();

                    Console.WriteLine("Имя успешно изменено, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 4)
                {
                    Console.WriteLine("Введите имя клиента: ");
                    bank.ShowClient();

                    Console.WriteLine("Клиент успешно показан, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }

                if (Choice == 5)
                {
                    bank.ShowAllClient();

                    Console.WriteLine("Клиенты успешно показаны, желаете продолжить?");
                    string otvet = Console.ReadLine();
                    if (otvet == "Да" || otvet == "да")
                    {
                        continue;
                    }
                    break;
                }
            }
        }
    }
}   