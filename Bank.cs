﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson7
{
    internal class Bank
    {
        public string Name_Bank;
        public string Address ;

        public Bank()
        {
            Name_Bank = "Стабильность";
            Address = "Проспект пушкина, дом Колотушкина";
        }

        List<Client> clientlist = new List<Client>();

        public void Add(string name, string surname, int age, double account_balance)
        {
           

            Client client = new Client(name, surname, age, account_balance);

            clientlist.Add(client);
        }
        public void Remove()
        {
            string SearchSurname = Console.ReadLine();

            for (int i = 0; i < clientlist.Count; i++)
            {
                if (SearchSurname == clientlist[i].Surname)
                {
                    clientlist.Remove(clientlist[i]);
                }
            }

        }
        public void ShowClient()
        {
            string SearchSurname = Console.ReadLine();

            for (int i = 0; i < clientlist.Count; i++)
            {
                if (SearchSurname == clientlist[i].Surname)
                {
                    Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    Console.WriteLine($"Фамилия {clientlist[i].Surname}");
                    Console.WriteLine($"Имя     {clientlist[i].Name}");
                    Console.WriteLine($"Возраст {clientlist[i].Age} лет");
                    Console.WriteLine($"Счет    {clientlist[i].Account_balance}");
                    Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                }
            }
        }

        public void ChangeBalance()
        {
            string SearchSurname = Console.ReadLine();

            for (int i = 0; i < clientlist.Count; i++)
            {
                if (SearchSurname == clientlist[i].Surname)
                {
                    Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    Console.WriteLine($"Текущий счет{clientlist[i].Account_balance}");
                    Console.WriteLine($"Введите новый счет");
                    double _Account_balance =Convert.ToDouble( Console.ReadLine());
                    clientlist[i].Account_balance = _Account_balance;
                    Console.WriteLine($"Текущий счет{clientlist[i].Account_balance}");
                    Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                }
            }

        }

        public void ShowAllClient()
        {
            clientlist.Sort((left, right) => left.Name.CompareTo(right.Name));

            for (int i = 0; i < clientlist.Count; i++)
            {
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                Console.WriteLine($"Фамилия {clientlist[i].Surname}");
                Console.WriteLine($"Имя     {clientlist[i].Name}");
                Console.WriteLine($"Возраст {clientlist[i].Age} лет");
                Console.WriteLine($"Счет    {clientlist[i].Account_balance}");
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
            }
        }

        public void ReClient()
        {
            string SearchSurname = Console.ReadLine();

            for (int i = 0; i < clientlist.Count; i++)
            {
                if (clientlist[i].Surname == SearchSurname)
                {
                    Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                    Console.WriteLine($"Фамилия {clientlist[i].Surname}");
                    Console.WriteLine($"Имя     {clientlist[i].Name}");
                    Console.WriteLine($"Возраст {clientlist[i].Age} лет");
                    Console.WriteLine($"Счет    {clientlist[i].Account_balance}");
                    Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");

                    Console.WriteLine("1. Изменить имя клиента");
                    Console.WriteLine("2. Изменить фамилию клиента");
                    Console.WriteLine("3. Изменить возраст клиента");
                    Console.WriteLine("4. Изменить счет клиента");

                    int choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Введите новое имя: ");
                            string name = Console.ReadLine();
                            clientlist[i].ReName(name);
                            break;

                        case 2:
                            Console.WriteLine("Введите новую фамилию: ");
                            string surname = Console.ReadLine();
                            clientlist[i].ReSurname(surname);
                            break;

                        case 3:
                            Console.WriteLine("Введите новый возраст: ");
                            int age = Convert.ToInt32(Console.ReadLine());
                            clientlist[i].ReAge(age);
                            break;

                        case 4:
                            Console.WriteLine("Введите новый баланс: ");
                            double balance = Convert.ToDouble(Console.ReadLine());
                            clientlist[i].ReBalance(balance);
                            break;

                            default: Console.WriteLine("Выберите от 1 до 4");
                            break;
                    }

                }

            }
        }


    }
    
}
